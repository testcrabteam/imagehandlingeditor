Version opportunities:
-Open image (supported formats: BMP, JPG, PNG);
-Coords rendering in real time system; (on image mouse hover)
-Undo/redo system; (CTRL + Z/CTRL + Y)
-Grayscale effect;
-Negative effect;
-Binarization effect with threshold slider and colorpicker;
-Black and white noise effect with power selection;
-Color noise effect with power selection;
-Noise linear filter;
-Noise median filter;
-Color histogram in real time tracking.
-Functions on testing:
-Image area selection;
-Save image on HDD.
-Cross screen resolutions support.

For opening editor you should open in your browser index.html file.

Supported browsers:
-Yandex browser;
-Google Chrome.