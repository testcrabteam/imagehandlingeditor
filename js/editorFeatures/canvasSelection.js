class CanvasSelector
{

  constructor(canvas)
  {
    this.canvas = canvas;

    this.selectionRectangle =
    {
      x: 0,
      y: 0,
      width: 0,
      height: 0
    };
    this.selectionStartCoords = null;

    this.update = this.SelectionUpdate.bind(this);
  }

  /*----------------------------------------------------------------------*/

  SelectionActivate()
  {
    this.ClearSelection(); //Clear prev selection
    var selector = this;

    var canvas = this.canvas;
    canvas.addEventListener("mousemove", this.update);
  }

  /*----------------------------------------------------------------------*/

  SelectionUpdate(event)
  {
    var context = this.canvas.getContext("2d");
    ClearSelectionRectangle(this.selectionRectangle, context);

    var mouseX = event.pageX;
    var mouseY = event.pageY;

    var canvas = this.canvas;
    var canvasRect = canvas.getBoundingClientRect();  //Canvas page coords

    var canvasX = mouseX - canvasRect.x;
    var canvasY = mouseY - canvasRect.y;

    if (this.selectionStartCoords)
    {
      var coords = this.selectionStartCoords;
      var selectionX = coords.x;
      var selectionY = coords.y;
      var selectionWidth = canvasX - coords.x;
      var selectionHeight = canvasY - coords.y;

      context.strokeRect(selectionX, selectionY, selectionWidth, selectionHeight);

      this.selectionRectangle =
      {
        x: selectionX,
        y: selectionY,
        width: selectionWidth,
        height: selectionHeight
      };
    }
    else
    {
      this.selectionStartCoords =
      {
        x: canvasX,
        y: canvasY
      };
    }

    function ClearSelectionRectangle(selectionRectangle, context)
    {
      var x = selectionRectangle.x;
      var y = selectionRectangle.y;
      var width = selectionRectangle.width;
      var height = selectionRectangle.height;

      context.clearRect(x, y, width, height);
    }
  }

  /*----------------------------------------------------------------------*/

  SelectionDeactivate()
  {
    var selector = this;
    var canvas = this.canvas;
    canvas.removeEventListener("mousemove", this.update);
  }

  /*----------------------------------------------------------------------*/

  ClearSelection()
  {
    var x = this.selectionRectangle.x;
    var y = this.selectionRectangle.y;
    var width = this.selectionRectangle.width;
    var height = this.selectionRectangle.height;
    var context = this.canvas.getContext("2d");

    this.selectionRectangle =
    {
      x: 0,
      y: 0,
      width: 0,
      height: 0
    };
    this.selectionStartCoords = null;
  }
}
