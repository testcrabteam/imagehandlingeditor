
var selectedArea;

function GetPixelBrightness(data, index)
{
  let brightness = 0.3*data[index] + 0.59*data[index + 1] + 0.11*data[index + 2];
  return brightness;
}

/*----------------------------------------------------------------------*/

function GetMaxOfArray(numArray)
{
  return Math.max.apply(null, numArray);
}

/*----------------------------------------------------------------------*/

function GetRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //Include max and min
}
