class Texture
{
  constructor(canvas, imageFile)
  {
    this.area = canvas;
    this.imageFile = imageFile;
  }

  Update()
  {
    if (this.area)
    {

      if (this.imageFile) UpdateImageFromFile(this.area, this.imageFile);
      else ClearArea(this.area);

    }

    //For drawing images

    function UpdateImageFromFile(area, image)
    {
      var fr = new FileReader();

      fr.onload = function()
      {
          DrawImageOnCanvas(area, fr.result);
      }

      fr.readAsDataURL(image); //Read input image src
    }
    function DrawImageOnCanvas(canvas, src)
    {
      if(canvas && src)
      {
        var ctx = canvas.getContext("2d");
        var img = new Image();

        img.onload = function()
        {
          DrawImageProp(ctx, img);
        }
        img.src = src;

      }
    }
    function DrawImageProp(ctx, img, x, y, w, h, offsetX, offsetY)
    {

        if (arguments.length === 2) {
            x = y = 0;
            w = ctx.canvas.width;   //Draw with canvas parameters
            h = ctx.canvas.height;
        }

        // default offset is center
        offsetX = typeof offsetX === "number" ? offsetX : 0.5;
        offsetY = typeof offsetY === "number" ? offsetY : 0.5;

        // keep bounds [0.0, 1.0]
        if (offsetX < 0) offsetX = 0;
        if (offsetY < 0) offsetY = 0;
        if (offsetX > 1) offsetX = 1;
        if (offsetY > 1) offsetY = 1;

        var iw = img.width,
            ih = img.height,
            r = Math.min(w / iw, h / ih),
            nw = iw * r,   // new prop. width
            nh = ih * r,   // new prop. height
            cx, cy, cw, ch, ar = 1;

        // decide which gap to fill
        if (nw < w) ar = w / nw;
        if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
        nw *= ar;
        nh *= ar;

        // calc source rectangle
        cw = iw / (nw / w);
        ch = ih / (nh / h);

        cx = (iw - cw) * offsetX;
        cy = (ih - ch) * offsetY;

        // make sure source rectangle is valid
        if (cx < 0) cx = 0;
        if (cy < 0) cy = 0;
        if (cw > iw) cw = iw;
        if (ch > ih) ch = ih;

        // fill image in dest. rectangle
        ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
    }
    function ClearArea(canvas)
    {
      var ctx = canvas.getContext("2d");
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
  };
}

class TextureEffect extends Texture
{
  constructor(textureEffect)
  {
    super(textureEffect.canvas, textureEffect.imageFile);
    this.effectObj = textureEffect.effectObj;
    this.params = textureEffect.params;
    this.imageArea = textureEffect.area;

    this.Update();
  }

  Update()
  {
    var effectObj = this.effectObj;
    var area = this.area;
    var params = this.params;

    var effectData =
    {
      effectObj: this.effectObj,
      canvas: this.area,
      params: this.params,
      area: this.imageArea
    };

    if (effectObj) ApplyEffect(effectData);
    else if (this.imageFile) super.Update();

    /*--------------------------------------------------------------------*/

    function ApplyEffect(effectData)
    {
      if (effectData)
      {
        var effectObj = effectData.effectObj;
        var canvas = effectData.canvas;
        var params = effectData.params;
        var area = effectData.area;

        var imageArea = (area) ? area :
        {
          x: 0,
          y: 0,
          width: canvas.width,
          height: canvas.height
        };

        var ctx = canvas.getContext("2d");
        var imageData = ctx.
        // getImageData(imageArea.x, imageArea.y, imageArea.width, imageArea.height);
        getImageData(0, 0, canvas.width, canvas.height);

        var data = imageData.data;

        //Update image
        //data = ApplyEffectOnAllData(data, effect, params);
        effectObj.imageData = data;
        effectObj.effectParams = params;
        effectObj.imageArea = imageArea;
        effectObj.canvas = canvas;

        data = effectObj.ApplyEffect();
        ctx.putImageData(imageData, 0, 0);

        return data;
      }
    }
  }
}
