
class ActionInfo
{
  constructor(input, output)
  {
    this.inputTexture = input;
    this.outputTexture = output;
  }
}

class Action
{
  constructor(input, output)
  {
    this.info = new ActionInfo(input, output);
    this.next = null;
    this.prev = null;

    Action.ActionSystemUpdateDispatcher();
  }

  Apply()
  {
    if(Texture)
    {
      // TODO: update system statement (not one texture update :)
      let info = this.info;

      info.inputTexture.Update();
      info.outputTexture.Update();
    }

    Action.ActionSystemUpdateDispatcher();
  }


  //Called every action applying
  static ActionSystemUpdateDispatcher()
  {

  }
}

class ActionStack
{

  constructor(initialAction)
  {
    this.back = initialAction;
    this.initial = initialAction;
  }

  Push(action)
  {

    if(action)
    {

      if(this.back)
      {
        this.back.next = action;
        action.prev = this.back;
        this.back = action;
      }
      else
      {
        this.back = action;
        action.next = null;
        action.prev = null;

      }

    }

  }

  // Pop()
  // {
  //
  //   if(this.back)
  //   {
  //
  //     if (this.back.next)
  //     {
  //       let prev = this.back.prev;
  //       prev.next = null;
  //       this.back = prev;
  //     }
  //     else
  //     {
  //       this.back = null;
  //     }
  //
  //   }
  //
  // }

  Undo()
  {
    if (this.back !== this.initial)
    {
      this.back = this.back.prev;
      this.back.Apply();
      console.log("Undo");
    }
  }

  Redo()
  {
    if (this.back && this.back.next)
    {
      this.back = this.back.next;
      this.back.Apply();
      console.log("Redo");
    }
  }

}
