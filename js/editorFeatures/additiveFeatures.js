/*----------------------------------------------------------------------*/

function DownloadImageFromCanvas(event, canvas)
{
  event.preventDefault();

  var url = canvas.toDataURL("image/jpg");
  document.write('<img src="'+url+'"/>');
}

/*----------------------------------------------------------------------*/

function CoordsRendering(event)
{
  var mouseX = event.pageX;
  var mouseY = event.pageY;

  var canvas = event.target;
  var canvasRect = canvas.getBoundingClientRect();  //Canvas page coords

  var canvasX = mouseX - canvasRect.x;
  var canvasY = mouseY - canvasRect.y;

  canvasX = canvasX.toFixed(2);
  canvasY = canvasY.toFixed(2);

  var coordsEl = document.querySelector(".canvasCoords");
  coordsEl.textContent = "X:" + canvasX + " Y: " + canvasY;
}

/*----------------------------------------------------------------------*/

function UpdateCanvasSize(textureObj)
{
  const defaultCanvasSize = 550;
  const defaultScreenWidth = 1920;

  const clientWidth = document.body.clientWidth;

  var resultSize = defaultCanvasSize * clientWidth / defaultScreenWidth;

  textureObj.area.width = resultSize;
  textureObj.area.height = resultSize;

  textureObj.Update();
}

/*----------------------------------------------------------------------*/

function GetChannelsValuesPickers(pickers)
{
  var channelsValues = [];

  pickers.forEach(function(picker)
  {
    var textValue = picker.value;
    var rgbObjValue = hexToRGB(textValue);
    channelsValues.push(rgbObjValue.r, rgbObjValue.g, rgbObjValue.b)
  });

  return channelsValues;
}

/*----------------------------------------------------------------------*/

function hexToRGB(h)
{
  let r = 0, g = 0, b = 0;

  // 3 digits
  if (h.length == 4) {
    r = "0x" + h[1] + h[1];
    g = "0x" + h[2] + h[2];
    b = "0x" + h[3] + h[3];

  // 6 digits
  }
  else if (h.length == 7)
  {
    r = "0x" + h[1] + h[2];
    g = "0x" + h[3] + h[4];
    b = "0x" + h[5] + h[6];
  }

  return {
    r: parseInt(r),
    g: parseInt(g),
    b: parseInt(b)
  }
}

/*----------------------------------------------------------------------*/

function MakeUndo(event, stack)
{
  let zCharCode = 26; //Ctrl z symbol code

  //CTRL + Z
  if (event.ctrlKey && event.charCode == zCharCode) stack.Undo();
}

/*----------------------------------------------------------------------*/

function MakeRedo(event, stack)
{
  let yCharCode = 25; //Ctrl y symbol code

  //CTRL + Y
  if (event.ctrlKey && event.charCode == yCharCode) stack.Redo();
}
