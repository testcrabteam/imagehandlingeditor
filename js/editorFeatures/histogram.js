function HistogramUpdate(histogramCanvas, textureCanvas, isColor)
{
  if (isColor)
  {
    brightness = GetBrightnessHistsObj(textureCanvas);
    UpdateHistogramCanvasOnHists(histogramCanvas, brightness);
  }
  else
  {
    brightness = GetBrightnessArr(textureCanvas);
    UpdateHistogramCanvas(histogramCanvas, brightness);
  }
}

/*----------------------------------------------------------------------*/

//Get brightness 255 array values
function GetBrightnessArr(canvas)
{
  var ctx = canvas.getContext("2d");
  var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);  // TODO:
  var data = imageData.data;

  let histBrightness = (new Array(256)).fill(0);
  let channelsCount = 4; //RGBA

  for (let i = 0; i < data.length; i += channelsCount)
  {
    let r = data[i];
    let g = data[i + 1];
    let b = data[i + 2];

    histBrightness[r]++;
    histBrightness[g]++;
    histBrightness[b]++;
  }

  return histBrightness;
}

/*----------------------------------------------------------------------*/

function GetBrightnessHistsObj(canvas)
{
  var ctx = canvas.getContext("2d");
  var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);  // TODO:
  var data = imageData.data;

  let histR = (new Array(256)).fill(0);
  let histG = (new Array(256)).fill(0);
  let histB = (new Array(256)).fill(0);

  let channelsCount = 4; //RGBA

  for (let i = 0; i < data.length; i += channelsCount)
  {
    let r = data[i];
    let g = data[i + 1];
    let b = data[i + 2];

    histR[r]++;
    histG[g]++;
    histB[b]++;
  }

  var hists =
  {
    r: histR,
    g: histG,
    b: histB
  };

  return hists;
}

/*----------------------------------------------------------------------*/

function UpdateHistogramCanvas(canvas, brightness)
{

  let maxBrightness = GetMaxBrightness(brightness);
  const brightnessValuesCount = 256;

  const ctx = canvas.getContext('2d');

  let guideHeight = 8;
  let startY = (canvas.height - guideHeight);

  let dx = canvas.width / brightnessValuesCount;
  let dy = startY / maxBrightness;

  ctx.lineWidth = dx;
  ctx.fillStyle = "#fff";
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  for (let i = 0; i < 256; i++)
  {
    let x = i * dx;
    ctx.strokeStyle = "#000000";
    ctx.beginPath();
    ctx.moveTo(x, startY);
    ctx.lineTo(x, startY - brightness[i] * dy);
    ctx.closePath();
    ctx.stroke();

    // Guide
    ctx.strokeStyle = 'rgb(' + i + ', ' + i + ', ' + i + ')';
    ctx.beginPath();
    ctx.moveTo(x, startY);
    ctx.lineTo(x, canvas.height);
    ctx.closePath();
    ctx.stroke();
  }
}

/*----------------------------------------------------------------------*/

function UpdateHistogramCanvasOnHists(canvas, hists)
{
  let maxBrightness = GetMaxBrightnessFromHists(hists);
  const brightnessValuesCount = 256;

  const ctx = canvas.getContext('2d');

  let guideHeight = 8;
  let startY = (canvas.height - guideHeight);

  let dx = canvas.width / brightnessValuesCount;
  let dy = startY / maxBrightness;

  ctx.lineWidth = dx;
  ctx.fillStyle = "#fff";
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  for (let i = 0; i < 256; i++)
  {
    let x = i * dx;

    // Red
    ctx.strokeStyle = "rgba(220,0,0,0.5)";
    ctx.beginPath();
    ctx.moveTo(x, startY);
    ctx.lineTo(x, startY - hists.r[i] * dy);
    ctx.closePath();
    ctx.stroke();

    // Green
    ctx.strokeStyle = "rgba(0,210,0,0.5)";
    ctx.beginPath();
    ctx.moveTo(x, startY);
    ctx.lineTo(x, startY - hists.g[i] * dy);
    ctx.closePath();
    ctx.stroke();

    // Blue
    ctx.strokeStyle = "rgba(0,0,255,0.5)";
    ctx.beginPath();
    ctx.moveTo(x, startY);
    ctx.lineTo(x, startY - hists.b[i] * dy);
    ctx.closePath();
    ctx.stroke();

    // Guide
    ctx.strokeStyle = 'rgb(' + i + ', ' + i + ', ' + i + ')';
    ctx.beginPath();
    ctx.moveTo(x, startY);
    ctx.lineTo(x, canvas.height);
    ctx.closePath();
    ctx.stroke();
  }
}

/*----------------------------------------------------------------------*/

function GetMaxBrightness(histBrightness)
{
  let maxBrightness = 0;

  for (let i = 1; i < 256; i++) {
    if (maxBrightness < histBrightness[i])
    {
      maxBrightness = histBrightness[i]
    }
  }

  return maxBrightness;
}

/*----------------------------------------------------------------------*/

function GetMaxBrightnessFromHists(hists)
{
  let maxBrightness = 0;

  for (let i = 0; i < 256; i++)
  {
    if (maxBrightness < hists.r[i]) maxBrightness = hists.r[i]
    else if (maxBrightness < hists.g[i]) maxBrightness = hists.g[i]
    else if (maxBrightness < hists.b[i]) maxBrightness = hists.b[i]
  }

  return maxBrightness;
}
