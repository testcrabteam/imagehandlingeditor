
function InitColorPicker()
{
  var colors = jsColorPicker('input.color', {
    customBG: '#000',
    readOnly: true,
    init: function(elm, colors){ // colors is a different instance (not connected to colorPicker)
      elm.style.backgroundColor = elm.value;
      elm.style.color = colors.rgbaMixCustom.luminance > 0.22 ? '#222' : '#ddd';
    },
  });
}

/*----------------------------------------------------------------------*/

function InitFileInput(stack,input, output)
{
  var fileInput = document.getElementById("fileInput");

  fileInput.addEventListener("change", function()
  {

    // TODO: REORGANIZE

    var file = fileInput.files[0];

    var inputData =
    {
      canvas: input,
      imageFile: file
    }

    var outputData =
    {
      canvas: output,
      imageFile: file
    }

    inputTexture = new TextureEffect(inputData);
    outputTexture = new TextureEffect(outputData);

    stack.Push(new Action(inputTexture, outputTexture));
  });
}

/*----------------------------------------------------------------------*/

function InitGrayscale(stack, input, output)
{
  var grayscaleButton = document.getElementById("grayscaleButton");

  grayscaleButton.addEventListener("click", function()
  {
    let grayscaleEffectObj = new GrayscaleImageEffect();
    ApplyEffectWithLogging(stack, grayscaleEffectObj, input, output);
  });
}

/*----------------------------------------------------------------------*/

function InitNegative(stack, input, output)
{
  var negativeButton = document.getElementById("negativeButton");

  negativeButton.addEventListener("click", function()
  {
    let negativeEffectObj = new NegativeImageEffect();
    ApplyEffectWithLogging(stack, negativeEffectObj, input, output);
  });
}

/*----------------------------------------------------------------------*/

function InitBinary(stack, input, output)
{
  var rangeLine = document.getElementById("rangeLine");

  rangeLine.addEventListener("change", function(event)
  {
    var parent = event.currentTarget.parentNode;
    var pickers = parent.querySelectorAll("input.color");

    if (pickers)
    {
      params = [];
      params[0] = rangeLine.value;

      channelsValues = GetChannelsValuesPickers(pickers);
      params = params.concat(channelsValues);

      let binaryImageEffect = new BinaryImageEffect();
      ApplyEffectWithLogging(stack, binaryImageEffect, input, output, null, params);
    }

  });
}

/*----------------------------------------------------------------------*/

function InitParticleNoise(stack, input, output)
{
  var particle = document.getElementById("particle");

  particle.addEventListener("change", function()
  {
    var value = particle.value;
    let particleEffect = new ParticleNoiseImageEffect();
    ApplyEffectWithLogging(stack, particleEffect, input, output, null, value);
  });
}

/*----------------------------------------------------------------------*/

function InitSoldNoise(stack, input, output)
{
  var noiseSelect = document.getElementById("noise");

  noiseSelect.addEventListener("change", function()
  {
    var value = noiseSelect.value;
    let noiseEffect = new SoldNoiseImageEffect()
    ApplyEffectWithLogging(stack, noiseEffect, input, output, null, value);
  });
}

/*----------------------------------------------------------------------*/

function InitLinearFilter(stack, input, output)
{
  var linearFilter = document.getElementById("linearFilter");

  linearFilter.addEventListener("click", function()
  {
    let linearFilterEffect = new LinearFilterImageEffect();
    ApplyEffectWithLogging(stack, linearFilterEffect, input, output, null, output.height);
  });
}

/*----------------------------------------------------------------------*/

function InitKirchFilter(stack, input, output)
{
  var kirchFilter = document.getElementById("kirchFilter");

  kirchFilter.addEventListener("click", function()
  {
    let kirchFilterEffect = new KirchFilterImageEffect();
    ApplyEffectWithLogging(stack, kirchFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitMedianFilter(stack, input, output)
{
  var medianFilter = document.getElementById("medianFilter");

  medianFilter.addEventListener("click", function()
  {
    let medianFilterEffect = new MedianFilterImageEffect();
    ApplyEffectWithLogging(stack, medianFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitLaplasFilter(stack, input, output)
{
  var laplasFilter = document.getElementById("laplasFilter");

  laplasFilter.addEventListener("click", function()
  {
    let laplasFilterEffect = new LaplasFilterImageEffect();
    ApplyEffectWithLogging(stack, laplasFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitRobertFilter(stack, input, output)
{
  var robertFilter = document.getElementById("robertFilter");

  robertFilter.addEventListener("click", function()
  {
    let robertFilterEffect = new RobertFilterImageEffect();
    ApplyEffectWithLogging(stack, robertFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitSobelFilter(stack, input, output)
{
  var sobelFilter = document.getElementById("sobelFilter");

  sobelFilter.addEventListener("click", function()
  {
    let sobelFilterEffect = new SobelFilterImageEffect();
    ApplyEffectWithLogging(stack, sobelFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitWalleceFilter(stack, input, output)
{
  var walleceFilter = document.getElementById("walleceFilter");

  walleceFilter.addEventListener("click", function()
  {
    let walleceFilterEffect = new WalleceFilterImageEffect();
    ApplyEffectWithLogging(stack, walleceFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitStatisticFilter(stack, input, output)
{
  var statisticFilter = document.getElementById("statisticFilter");

  statisticFilter.addEventListener("click", function()
  {
    let statisticFilterEffect = new StatisticFilterImageEffect();
    ApplyEffectWithLogging(stack, statisticFilterEffect, input, output, null, output.width);
  });
}

/*----------------------------------------------------------------------*/

function InitKlasterEffect(stack, input, output)
{
  var klasterEffect = document.getElementById("klasterEffect");

  klasterEffect.addEventListener("click", function()
  {
    let klasterEffect = new KlasterImageEffect();
    ApplyEffectWithLogging(stack, klasterEffect, input, output, null, output.width);  
  });
}



/*----------------------------------------------------------------------*/

function InitHistogram(output)
{
  var histogramCanvas = document.getElementById("histogramCanvas");
  var histModes = document.getElementsByName("histMode");

  Action.ActionSystemUpdateDispatcher = function()
  {
    var histMode = document.histForm.histMode;
    var histRadioValue = histMode.value;
    var isColor = (histRadioValue == "color") ? true : false;

    //From histogram.js
    HistogramUpdate(histogramCanvas, output, isColor);
  }

  histModes.forEach(function(item)
  {
    item.addEventListener("change", function(event)
    {
      var value = event.target.value;
      var isColor = (value == "color") ? true : false;

      //From histogram.js
      HistogramUpdate(histogramCanvas, output, isColor);
    });
  });
}

/*----------------------------------------------------------------------*/

function InitSaveFile(output)
{
  var saveButton = document.querySelector("#fileForm .download");

  saveButton.addEventListener("click", function(event)
  {
    DownloadImageFromCanvas(event, output); //Additive feature
  });
}

/*----------------------------------------------------------------------*/

function InitCanvasSelection(stack, input, inputTexture, output)
{
  //Selection (use canvasSelection.js)
  var canvasSelector = new CanvasSelector(input);

  input.addEventListener("mousedown", function(event)
  {
    canvasSelector.SelectionActivate();
    inputTexture.Update();
  });

  /*----------------------------------------------------------------------*/

  input.addEventListener("mouseup", function(event)
  {
    canvasSelector.SelectionDeactivate();
    selectedArea = canvasSelector.selectionRectangle;

    if (selectedArea.width > 0)
    {
      let copyEffect = new CopyImageEffect();
      ApplyEffectWithLogging(stack, copyEffect, input, output);
    }
    else
    {
      UpdateWidthSelectedEffectArea(input, output);
      selectedArea = null;

      function UpdateWidthSelectedEffectArea(input, output)
      {
        //Get input texture data
        let ctx = input.getContext("2d");
        inputImageData = ctx.
        getImageData(0, 0, input.width, input.height);
        var inputData = inputImageData.data;

        //Get output texture data
        ctx = output.getContext("2d");
        var outputImageData = ctx.
        getImageData(0, 0, output.width, output.height);
        var outputData = outputImageData.data;

        //Redrawing with selected area
        for (let i = 0; i < outputData.length; i++)
        {
          if (outputData[i] == 194 &&
              outputData[i + 1] == 194 &&
              outputData[i + 2] == 194)
          {
            outputData[i] = inputData[i];
            outputData[i + 1] = inputData[i + 1];
            outputData[i + 2] = inputData[i + 2];
          }
        }

        //Update output
        ctx.putImageData(outputImageData, 0, 0);
      }
    }

  });

  /*----------------------------------------------------------------------*/

  input.addEventListener("click", function(event)
  {


    //
    // let copyEffect = new CopyImageEffect();
    // ApplyEffectWithLogging(stack, copyEffect, input, output);
  });
}

/*----------------------------------------------------------------------*/

function InitCoordsRendering(input)
{
  input.addEventListener("mouseover", function()
  {
    input.addEventListener("mousemove", CoordsRendering);
  });

  input.addEventListener("mouseout", function()
  {
    input.removeEventListener("mousemove", CoordsRendering);
    var coordsEl = document.querySelector(".canvasCoords");
    coordsEl.textContent = "Наведите на картинку!";
  });
}

/*----------------------------------------------------------------------*/

function InitCanvasSizeUpdate(inputTexture, outputTexture)
{

  /*Used with additiveFeatures.js*/

  document.body.onresize = function()
  {
    UpdateCanvasSize(inputTexture);
    UpdateCanvasSize(outputTexture);
  };

  //First load update
  UpdateCanvasSize(inputTexture);
  UpdateCanvasSize(outputTexture);
}

/*----------------------------------------------------------------------*/

function InitUndoRedoSystem(stack)
{
  //Used additional features
  document.addEventListener("keypress", function(event)
  {
    MakeUndo(event, stack)
  });
  document.addEventListener("keypress", function(event)
  {
    MakeRedo(event, stack)
  });
}

/*----------------------------------------------------------------------*/
