class SoldNoiseImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Create grayscale effect
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;
    let scalar = this.effectParams;

    //from global features
    var brightness = GetPixelBrightness(imageData, dataIndex);

    if (brightness > 100) // Makes BW image
    {
      imageData[dataIndex] = 255;
      imageData[dataIndex + 1] = 255;  //First three channels
      imageData[dataIndex + 2] = 255;
    }
    else
    {
      imageData[dataIndex] = 0;
      imageData[dataIndex + 1] = 0;  //Last three channels
      imageData[dataIndex + 2] = 0;
    }

    var chance = Math.floor(Math.random() * 100)+1;
    var prblity = scalar * 100;

    if (chance <= prblity)
    {
      if (imageData[dataIndex] == 0)
      {
        imageData[dataIndex] = 255;
        imageData[dataIndex + 1] = 255;
        imageData[dataIndex + 2] = 255;
      }
      else
      {
        imageData[dataIndex] = 0;
        imageData[dataIndex + 1] = 0;
        imageData[dataIndex + 2] = 0;
      }
    }
  }
}
