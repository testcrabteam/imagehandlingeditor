class NegativeImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Create grayscale effect
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;

    imageData[dataIndex] = 255 - imageData[dataIndex];
    imageData[dataIndex + 1] = 255 - imageData[dataIndex + 1];
    imageData[dataIndex + 2] = 255 - imageData[dataIndex + 2];

  }


}
