class KlasterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //General effect applying
  ApplyEffect()
  {
    const channelsCount = 4; //RGBA

    let imageData = this.imageData;
    let effectParams = this.effectParams;

    // let angleArr = this.GetAreaAngleIndexesArr();
    //
    // for (var i = 0; i < imageData.length; i += channelsCount)
    // {
    //   if (this.IsIndexInArea(angleArr, i))
    //   {
    //     this.ApplyEffectOnPixel(i)
    //   }
    //   else
    //   {
    //     imageData[i] = 194;
    //     imageData[i + 1] = 194;
    //     imageData[i + 2] = 194;
    //   }
    // }

    const maxBlocksCount = 16;

    let blocks = this.GetAperturePixelsArr();
    let angleArr = this.GetAreaAngleIndexesArr();
    let clustersCenters;
    let clusters;

    for (let i = 2; i < maxBlocksCount; i++)
    {
      clusters = this.GetClustersOnBlocks(i);
      clustersCenters = this.GetClustersCenters(clusters);
      console.log(clustersCenters);

    }

    for (let i = 0; i < clusters.length; i++)
    {
      clusters[i].forEach((item, j) => {
        imageData[item] = 194;
        imageData[item + 1] = 194;
        imageData[item + 2] = 194;
      });

    }

    return imageData;
  }

  /*----------------------------------------------------------------------*/

  GetAperturePixelsArr()
  {
    let imageData = this.imageData;
    let aperturePixels = [];
    const blockNeedSize = 4;
    const channelsCount = 4;

    for (let i = 0; i < imageData.length; i += channelsCount)
    {
      let block = this.GetApertureArrChannels(i);

      //Is full block
      if (block.length == blockNeedSize) aperturePixels.push(block);
    }

    return aperturePixels;
  }

  /*----------------------------------------------------------------------*/

  GetApertureArrChannels(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;
    let effectParams = this.effectParams;

    const channelsCount = 4;

    var channels = [];

    //Center
    channels.push(this.GetDataObjByIndex(dataIndex));

    //Forward
    channels.push(this.GetDataObjByIndex(dataIndex + channelsCount));

    //DownRight
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount + channelsCount));

    //Down
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount));

    return channels;

  }

  /*----------------------------------------------------------------------*/

  GetClustersOnBlocks(clustersCount)
  {
    let angleArr = this.GetAreaAngleIndexesArr();
    let angleArrLength = angleArr.length;
    let clusters = [];

    let leftUpAngle, rightUpAngle, rightDownAngle, leftDownAngle;

    //cluster = []

    switch(clustersCount)
    {
      case 2:
        leftUpAngle = angleArr[0].start;
        rightDownAngle = angleArr[angleArrLength - 1].end;

        clusters =
        [
          [leftUpAngle],
          [rightDownAngle]
        ];

        break;

      case 3:
        leftUpAngle = angleArr[0].start;
        rightUpAngle = angleArr[0].end;
        rightDownAngle = angleArr[angleArrLength - 1].end;

        clusters =
        [
          [leftUpAngle],
          [rightUpAngle],
          [rightDownAngle]
        ];
        break;

      case 4:
        leftUpAngle = angleArr[0].start;
        rightUpAngle = angleArr[0].end;
        leftDownAngle = angleArr[angleArrLength - 1].start;
        rightDownAngle = angleArr[angleArrLength - 1].end;

        clusters =
        [
          [leftUpAngle],
          [rightUpAngle],
          [leftDownAngle],
          [rightDownAngle]
        ];
        break;

      default:

        if (clustersCount > 4)
        {
          leftUpAngle = angleArr[0].start;
          rightUpAngle = angleArr[0].end;
          leftDownAngle = angleArr[angleArrLength - 1].start;
          rightDownAngle = angleArr[angleArrLength - 1].end;

          clusters =
          [
            [leftUpAngle],
            [rightUpAngle],
            [leftDownAngle],
            [rightDownAngle]
          ];

          let minClusterCenter = angleArr[1].start + 3;
          let maxClusterCenter = angleArr[angleArrLength - 2].end - 3;

          for (let i = 5; i <= clustersCount; i++)
          {
            let clusterCenter =
                    GetRandomIntInclusive(minClusterCenter, maxClusterCenter);
            clusterCenter = Math.floor(clusterCenter / 4);
            clusters.push([clusterCenter]);
          }
        }

        break;
    }

    return clusters;
  }

  //Return RGB objects array
  GetClustersCenters(clusters)
  {
    let centers = [];

    clusters.forEach((item, i) => {
      let length = item.length;
      let sumR = 0;
      let sumG = 0;
      let sumB = 0;

      //console.log(item);

      //Calculate center
      item.forEach((pixelIndex, j) => {
        sumR += pixelIndex / length;
        sumG += (pixelIndex + 1) / length;
        sumB += (pixelIndex + 2) / length;
      });

      //Result RGB obj
      let centerObj =
      {
        r: sumR,
        g: sumG,
        b: sumB,
      }

      centers.push(centerObj);
    });

    return centers;
  }

  //Implements on inherited effects
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;
    let pixelChannels = this.GetPixelWithApertureEffect(dataIndex);

    imageData[dataIndex] = pixelChannels.r;
    imageData[dataIndex + 1] = pixelChannels.g;
    imageData[dataIndex + 2] = pixelChannels.b;
  }

  //Determine
  GetEffectResultInAperture(appertureChannels)
  {

  }
}
