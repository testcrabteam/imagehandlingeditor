class ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    this.imageData = imageData;
    this.effectParams = effectParams;
    this.imageArea = imageArea;
    this.canvas = canvas;
  }

  GetAreaAngleIndexesArr()
  {
    let area = this.imageArea;
    let canvas = this.canvas;
    let anglesArr = [];

    let startY = Math.trunc(area.y) * Math.trunc(canvas.width);

    const channelsCount = 4;  //RGBA

    //Calc angle indexes
    for (let i = 0; i < area.height; i++)
    {
      let startX = area.x + canvas.width * i;
      let startIndex = Math.trunc((startY + startX) * channelsCount);
      let endIndex = Math.trunc(startIndex + area.width * channelsCount);

      anglesArr[i] =
      {
        start: startIndex,
        end: endIndex
      };
    }

    return anglesArr;
  }

  IsIndexInArea(angleArr, dataIndex)
  {
    for (let i = 0; i < angleArr.length; i++)
    {
      let angle = angleArr[i];

      if (dataIndex >= angle.start && dataIndex <= angle.end)
      {
        return true;
      }
    }
    return false;
  }

  DebugPixel(i)
  {
    let imageData = this.imageData;

    imageData[i] = 150;
    imageData[i + 1] = 0;
    imageData[i + 2] = 0;

  }

  //General effect applying
  ApplyEffect()
  {
    const channelsCount = 4; //RGBA

    let imageData = this.imageData;
    let effectParams = this.effectParams;

    let angleArr = this.GetAreaAngleIndexesArr();

    for (var i = 0; i < imageData.length; i += channelsCount)
    {
      if (this.IsIndexInArea(angleArr, i))
      {
        this.ApplyEffectOnPixel(i)
      }
      else
      {
        imageData[i] = 194;
        imageData[i + 1] = 194;
        imageData[i + 2] = 194;
      }
    }

    return imageData;
  }

  //Implements on inherited effects
  ApplyEffectOnPixel(dataIndex)
  {

  }


}
