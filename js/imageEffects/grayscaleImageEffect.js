class GrayscaleImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Create grayscale effect
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;

    //from global features
    let grayScalar = GetPixelBrightness(imageData, dataIndex);

    imageData[dataIndex] = grayScalar;
    imageData[dataIndex + 1] = grayScalar;
    imageData[dataIndex + 2] = grayScalar;
  }


}
