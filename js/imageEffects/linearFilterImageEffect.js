class LinearFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Determine linear filtereffect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {
    var resultChannels =
    {
      r: 0,
      g: 0,
      b: 0
    };

    appertureChannels.forEach((item, i) =>
    {
      if (item)
      {
        resultChannels.r += (item.r / appertureChannels.length);
        resultChannels.g += (item.g / appertureChannels.length);
        resultChannels.b += (item.b / appertureChannels.length);
      }
    });

    return resultChannels;
  }
}
