class CopyImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }



  //Apply copy effect
  ApplyEffectOnPixel(dataIndex)
  {
    let data = this.imageData;

    data[dataIndex] = data[dataIndex];
    data[dataIndex + 1] = data[dataIndex + 1];
    data[dataIndex + 2] = data[dataIndex + 2];
  }
}
