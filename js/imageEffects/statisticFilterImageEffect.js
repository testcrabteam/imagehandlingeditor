class StatisticFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Apply aperture effect, final
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;
    let brightnessScale = this.GetPixelWithApertureEffect(dataIndex);
    //console.log(brightnessScale);

    ScalePixelBrightness(imageData, dataIndex, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex + 4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex - 4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex - imgWidth*4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex + imgWidth*4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex - imgWidth*4 - 4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex - imgWidth*4 + 4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex + imgWidth*4 - 4, brightnessScale);
    ScalePixelBrightness(imageData, dataIndex + imgWidth*4 + 4, brightnessScale);

    function ScalePixelBrightness(data, dataIndex, scaler)
    {
      if (dataIndex >= 0 && dataIndex < data.length)
      {
        data[dataIndex] *= scaler;
        data[dataIndex + 1] *= scaler;
        data[dataIndex + 2] *= scaler;
      }
    }

  }

  /*----------------------------------------------------------------------*/

  //Determine linear filtereffect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {
    let brightnessArr = [];
    appertureChannels.forEach((item, i) =>
    {
      if (item)
      {
        brightnessArr[i] = (0.3 * item.r + 0.59 * item.g + 0.11 * item.b) / 100;
      }
      else brightnessArr[i] = 0;
    });

    let averageValue = 0;
    brightnessArr.forEach((item, i) =>
    {
      averageValue += item / brightnessArr.length;
    });
    //console.log(averageValue);

    let averageDeltaValue = 0;
    brightnessArr.forEach((item, i) =>
    {
      averageDeltaValue += Math.pow(item - averageValue, 2) / brightnessArr.length;
    });

    let averageSqrValue = Math.sqrt(averageDeltaValue) + 0.4;

    return averageSqrValue;
  }
}
