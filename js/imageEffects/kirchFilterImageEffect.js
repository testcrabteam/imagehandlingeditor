class KirchFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  /*----------------------------------------------------------------------*/

  GetApertureArrChannels(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;
    let effectParams = this.effectParams;

    const channelsCount = 4;

    var channels = [];

    //UpLeft
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount - channelsCount));

    //Up
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount));

    //UpRight
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount + channelsCount));

    //Forward
    channels.push(this.GetDataObjByIndex(dataIndex + channelsCount));

    //DownRight
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount + channelsCount));

    //Down
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount));

    //DownLeft
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount - channelsCount));

    //Backward
    channels.push(this.GetDataObjByIndex(dataIndex - channelsCount));

    return channels;

  }

  //Determine linear filtereffect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {
    let brightnessArr = [];

    //Clear null elements
    var j = 0;
    while (j < appertureChannels.length)
    {
      if (appertureChannels[j]) j++;
      else appertureChannels.splice(j, 1);
    }

    var resultChannels;

    //Aperture must be full
    if (appertureChannels.length == 8)
    {
      appertureChannels.forEach((item, i) =>
      {
        brightnessArr[i] = 0.3 * item.r + 0.59 * item.g + 0.11 * item.b;
      });
      //console.log(appertureChannels);
      //console.log(brightnessArr);

      let s, t;
      let resultDeltaArr = [];

      for (let i = 0; i < brightnessArr.length; i++)
      {

        s = brightnessArr[i]
        + brightnessArr[AddMod8(i, 1)] + brightnessArr[AddMod8(i, 2)];

        t = brightnessArr[AddMod8(i, 3)]
        + brightnessArr[AddMod8(i, 4)] + brightnessArr[AddMod8(i, 5)]
        + brightnessArr[AddMod8(i, 6)] + brightnessArr[AddMod8(i, 7)];

        resultDeltaArr[i] = Math.abs(5*s - 3*t) / 5;
        //console.log(s);
        //console.log(t);
      }

      let resultBrightness = GetMaxOfArray(resultDeltaArr);

      if (resultBrightness > 255)
      {
        if (this.lastChannel < resultBrightness) resultBrightness = 255;
        else resultBrightness = 0;
      }

      this.lastChannel = resultBrightness;

      resultChannels =
      {
        r: resultBrightness,
        g: resultBrightness,
        b: resultBrightness,
      };
    }
    else
    {
      resultChannels = null;
    }
    return resultChannels;

    /*----------------------------------------------------------------------*/

    function AddMod8(a, b)
    {
      let summ = a + b;
      if (summ > 7) summ = summ - 8;
      return summ;
    }
  }
}
