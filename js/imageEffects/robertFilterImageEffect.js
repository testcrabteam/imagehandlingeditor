class RobertFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  /*----------------------------------------------------------------------*/

  GetApertureArrChannels(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;
    let effectParams = this.effectParams;

    const channelsCount = 4;

    var channels = [];

    //Center
    channels.push(this.GetDataObjByIndex(dataIndex));

    //Forward
    channels.push(this.GetDataObjByIndex(dataIndex + channelsCount));

    //Down
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount));

    //DownRight
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount + channelsCount));

    return channels;

  }

  //Determine linear filtereffect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {
    let brightnessArr = [];
    appertureChannels.forEach((item, i) =>
    {
      if (item)
      {
        brightnessArr[i] = 0.3 * item.r + 0.59 * item.g + 0.11 * item.b;
      }
      else brightnessArr[i] = 0;
    });

    let num1 = Math.pow(brightnessArr[0] - brightnessArr[3], 2);
    let num2 = Math.pow(brightnessArr[1] - brightnessArr[2], 2);

    let resultBrightness = Math.sqrt(num1 + num2);

    var resultChannels =
    {
      r: resultBrightness,
      g: resultBrightness,
      b: resultBrightness,
    };

    return resultChannels;
  }
}
