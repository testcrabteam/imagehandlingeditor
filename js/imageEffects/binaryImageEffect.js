class BinaryImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Create grayscale effect
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;
    let params = this.effectParams;
    let threshold = params[0]; //From 0 to 255

    //from global features
    let brightness = GetPixelBrightness(imageData, dataIndex);

    if (brightness <= threshold)
    {
      imageData[dataIndex] = params[1];
      imageData[dataIndex + 1] = params[2];    //First three channels
      imageData[dataIndex + 2] = params[3];
    }
    else
    {
      imageData[dataIndex] = params[4];
      imageData[dataIndex + 1] = params[5];    //Last three channels
      imageData[dataIndex + 2] = params[6];
    }
  }


}
