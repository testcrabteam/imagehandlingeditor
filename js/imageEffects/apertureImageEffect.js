class ApertureImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
    this.lastChannel;
  }

  //Apply aperture effect, final
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;
    let pixelChannels = this.GetPixelWithApertureEffect(dataIndex);

    imageData[dataIndex] = pixelChannels.r;
    imageData[dataIndex + 1] = pixelChannels.g;
    imageData[dataIndex + 2] = pixelChannels.b;

  }

  GetPixelWithApertureEffect(dataIndex)
  {
    let imageData = this.imageData;
    let params = this.effectParams;

    var channels = this.GetApertureArrChannels(dataIndex);

    //Get RGB object
    var resultChannels = this.GetEffectResultInAperture(channels);
    if (resultChannels == null)
    {
      resultChannels =
      {
        r: imageData[dataIndex],
        g: imageData[dataIndex],
        b: imageData[dataIndex]
      };
    }

    return resultChannels;
  }

  /*----------------------------------------------------------------------*/

  //Can be override!
  GetApertureArrChannels(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;

    const channelsCount = 4;

    var channels = [];

    //Center
    channels.push(this.GetDataObjByIndex(dataIndex));

    //Forward
    channels.push(this.GetDataObjByIndex(dataIndex + channelsCount));

    //Backward
    channels.push(this.GetDataObjByIndex(dataIndex - channelsCount));

    //Up
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount));

    //Down
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount));

    //UpLeft
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount - channelsCount));

    //UpRight
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount + channelsCount));

    //DownLeft
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount - channelsCount));

    //DownRight
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount + channelsCount));

    return channels;

  }

  /*----------------------------------------------------------------------*/

  GetDataObjByIndex(dataIndex)
  {
    let imageData = this.imageData;

    if (dataIndex >= 0 && dataIndex < imageData.length)
    {
      var obj =
      {
        r: imageData[dataIndex],
        g: imageData[dataIndex + 1],
        b: imageData[dataIndex + 2]
      }
      return obj;
    }
    return null;
  }

  /*----------------------------------------------------------------------*/

  //Determine effect applying on pixel in aperture center, must be override
  GetEffectResultInAperture(appertureChannels)
  {

  }
}
