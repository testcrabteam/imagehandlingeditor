class SobelFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  /*----------------------------------------------------------------------*/

  //Can be override!
  GetApertureArrChannels(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;

    const channelsCount = 4;

    var channels = [];

    //UpLeft
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount - channelsCount));

    //Up
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount));

    //UpRight
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount + channelsCount));

    //Backward
    channels.push(this.GetDataObjByIndex(dataIndex - channelsCount));

    //Center
    channels.push(this.GetDataObjByIndex(dataIndex));

    //Forward
    channels.push(this.GetDataObjByIndex(dataIndex + channelsCount));

    //DownLeft
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount - channelsCount));

    //Down
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount));

    //DownRight
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount + channelsCount));

    return channels;

  }

  /*----------------------------------------------------------------------*/

  //Determine linear filtereffect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {
    let brightnessArr = [];

    //Clear null elements
    var j = 0;
    while (j < appertureChannels.length)
    {
      if (appertureChannels[j]) j++;
      else appertureChannels.splice(j, 1);
    }

    var resultChannels;

    if (appertureChannels.length == 9)
    {
      appertureChannels.forEach((item, i) =>
      {
        brightnessArr[i] = 0.3 * item.r + 0.59 * item.g + 0.11 * item.b;
        brightnessArr[i] = item.r;
      });

      let x1 = brightnessArr[0] + 2*brightnessArr[1] + brightnessArr[2];
      let x2 = brightnessArr[6] + 2*brightnessArr[7] + brightnessArr[8];
      let x = x1 - x2;
      //console.log(x1 + "-" + x2 + "=" + x);

      let y1 = brightnessArr[0] + 2*brightnessArr[3] + brightnessArr[6];
      let y2 = brightnessArr[2] + 2*brightnessArr[5] + brightnessArr[8];
      let y = y1 - y2;
      //console.log(y1 + "-" + y2 + "=" + y);

      let resultBrightness = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

      //console.log(resultBrightness);

      if (resultBrightness > 255)
      {
        if (this.lastChannel < resultBrightness) resultBrightness = 255;
        else resultBrightness = 0;
      }

      this.lastChannel = resultBrightness;

      var resultChannels =
      {
        r: resultBrightness,
        g: resultBrightness,
        b: resultBrightness,
      };
    }
    else
    {
      resultChannels = null;
    }

    return resultChannels;

  }
}
