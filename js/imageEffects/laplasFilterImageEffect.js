  class LaplasFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  GetApertureArrChannels(dataIndex)
  {
    let imageData = this.imageData;
    let imgWidth = this.effectParams;
    let effectParams = this.effectParams;

    const channelsCount = 4;

    var channels = [];

    //UpLeft
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount - channelsCount));

    //Up
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount));

    //UpRight
    channels.push(this.GetDataObjByIndex(dataIndex - imgWidth*channelsCount + channelsCount));

    //Backward
    channels.push(this.GetDataObjByIndex(dataIndex - channelsCount));

    //Center
    channels.push(this.GetDataObjByIndex(dataIndex));

    //Forward
    channels.push(this.GetDataObjByIndex(dataIndex + channelsCount));

    //DownLeft
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount - channelsCount));

    //Down
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount));

    //DownRight
    channels.push(this.GetDataObjByIndex(dataIndex + imgWidth*channelsCount + channelsCount));

    return channels;
  }


  /*----------------------------------------------------------------------*/

  //Determine linear filtereffect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {

    let brightnessArr = [];

    //Clear null elements
    var j = 0;
    while (j < appertureChannels.length)
    {
      if (appertureChannels[j]) j++;
      else appertureChannels.splice(j, 1);
    }

    var resultChannels;

    if (appertureChannels.length == 9)
    {
      //console.log("Test");
      appertureChannels.forEach((item, i) =>
      {
        brightnessArr[i] = 0.3 * item.r + 0.59 * item.g + 0.11 * item.b;
      });

      let laplasArr = [-1, -2, -1, -2, 12, -2, -1, -2, -1];
      let resultArr = [];

      brightnessArr.forEach((item, i) =>
      {
        resultArr[i] = brightnessArr[i] * laplasArr[i];
      });

      let resultBrightness = 0;
      resultArr.forEach((item, i) =>
      {
        resultBrightness += item;
      });

      var resultChannels =
      {
        r: resultBrightness,
        g: resultBrightness,
        b: resultBrightness,
      };
    }
    else
    {
      resultChannels = null;
    }

    return resultChannels;
  }
}
