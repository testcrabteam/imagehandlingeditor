class ParticleNoiseImageEffect extends ImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Create grayscale effect
  ApplyEffectOnPixel(dataIndex)
  {
    let imageData = this.imageData;
    let params = this.effectParams;

    let scalar = params;
    let chance = Math.floor(Math.random() * 100)+1;
    let prblity = scalar*100;

    if (chance <= prblity)
    {
        imageData[dataIndex] -= 50;
        imageData[dataIndex + 1] -= 50;
        imageData[dataIndex + 2] -= 50;
    }
  }
}
