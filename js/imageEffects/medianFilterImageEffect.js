class MedianFilterImageEffect extends ApertureImageEffect
{
  constructor(imageData, effectParams, imageArea, canvas)
  {
    super(imageData, effectParams, imageArea, canvas);
  }

  //Determine median filter effect applying on pixel in aperture center
  GetEffectResultInAperture(appertureChannels)
  {
    var resultChannels =
    {
      r: 0,
      g: 0,
      b: 0
    };

    //Clear null elements
    var j = 0;
    while (j < appertureChannels.length)
    {
      if (appertureChannels[j]) j++;
      else appertureChannels.splice(j, 1);
    }

    //Sort by average brightness
    appertureChannels = appertureChannels.sort(function (a, b)
    {
      var aBrightness = (a.r + a.g + a.b) / 3;
      var bBrightness = (b.r + b.g + b.b) / 3;

      if (aBrightness > bBrightness) {
        return 1;
      }
      if (aBrightness < bBrightness) {
        return -1;
      }
      //a equal b
      return 0;
    });

    var medianIndex = Math.ceil(appertureChannels.length / 2 - 1);
    var medianValue = appertureChannels[medianIndex];

    resultChannels.r = medianValue.r;
    resultChannels.g = medianValue.g;
    resultChannels.b = medianValue.b;

    return resultChannels;
  }
}
