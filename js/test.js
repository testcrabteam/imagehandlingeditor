
function GetApertureArrChannels(dataIndex)
{
  let imageData =
  [
    0, 0, 0, 255, 30, 30, 30, 255, 60, 60, 60, 255, 90, 90, 90, 255, 120, 120, 120, 255, 150, 150, 150, 255,
    35, 60, 90, 255, 120, 255, 120, 255, 15, 15, 15, 255, 1, 1, 1, 255, 200, 200, 200, 255, 0, 1, 2, 255,
    180, 140, 100, 255, 70, 40, 10, 255, 65, 65, 65, 255, 70, 70, 70, 255, 135, 150, 165, 255, 0, 0, 0, 255
  ];
  let imgWidth = 6;

  const channelsCount = 4;

  var channels = [];

  //UpLeft
  channels.push(this.GetDataObjByIndex(imageData, dataIndex - imgWidth*channelsCount - channelsCount));

  //Up
  channels.push(this.GetDataObjByIndex(imageData, dataIndex - imgWidth*channelsCount));

  //UpRight
  channels.push(this.GetDataObjByIndex(imageData, dataIndex - imgWidth*channelsCount + channelsCount));

  //Backward
  channels.push(this.GetDataObjByIndex(imageData, dataIndex - channelsCount));

  //Center
  channels.push(this.GetDataObjByIndex(imageData, dataIndex));

  //Forward
  channels.push(this.GetDataObjByIndex(imageData, dataIndex + channelsCount));

  //DownLeft
  channels.push(this.GetDataObjByIndex(imageData, dataIndex + imgWidth*channelsCount - channelsCount));

  //Down
  channels.push(this.GetDataObjByIndex(imageData, dataIndex + imgWidth*channelsCount));

  //DownRight
  channels.push(this.GetDataObjByIndex(imageData, dataIndex + imgWidth*channelsCount + channelsCount));

  console.log(channels);

  return channels;

}

/*----------------------------------------------------------------------*/

function GetDataObjByIndex(imageData, dataIndex)
{
  if (dataIndex >= 0 && dataIndex < imageData.length)
  {
    var obj =
    {
      r: imageData[dataIndex],
      g: imageData[dataIndex + 1],
      b: imageData[dataIndex + 2]
    }
    return obj;
  }
  return null;
}

document.addEventListener("DOMContentLoaded", function()
{
  for (let i = 0; i < 72; i += 4)
  {
    GetApertureArrChannels(i);
  }
})
