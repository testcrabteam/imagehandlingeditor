/*----------------------------------------------------------------------*/

function ApplyEffectWithLogging(stack, effectObj, beforeArea, afterArea, imageArea, params)
{
  if (stack && effectObj && beforeArea && afterArea)
  {
    var info = GetLastEffectInfo(stack);
    var file = info.file;

    var inputData =
    {
      canvas: beforeArea,
      imageFile: file
    };

    var outputData =
    {
      canvas: afterArea,
      imageFile: file,
      effectObj: effectObj,
      params: params,
      area: selectedArea
    };

    var inputEffect = new TextureEffect(inputData);
    var outputEffect = new TextureEffect(outputData);

    var action = new Action(inputEffect, outputEffect);
    stack.Push(action);
  }

  /*--------------------------------------------------------------------*/

  function GetLastEffectInfo(stack)
  {
    var back = stack.back;
    var info = back.info;
    var output = info.outputTexture;

    var effect = output.effect;
    var outputFile = output.imageFile;

    var info =
    {
      effect: effect,
      file: outputFile
    }

    return info;
  }
}

/*----------------------------------------------------------------------*/
/*Init functions uses init.js!*/

function InitEditorEffectComponents(stack, input, output)
{
  InitColorPicker();
  InitFileInput(stack, input, output);
  InitGrayscale(stack, input, output);
  InitNegative(stack, input, output);
  InitBinary(stack, input, output);
  InitParticleNoise(stack, input, output);
  InitSoldNoise(stack, input, output);
  InitLinearFilter(stack, input, output);
  InitMedianFilter(stack, input, output);
  InitKirchFilter(stack, input, output);
  InitLaplasFilter(stack, input, output);
  InitRobertFilter(stack, input, output);
  InitSobelFilter(stack, input, output);
  //InitWalleceFilter(stack, input, output);
  InitStatisticFilter(stack, input, output);
  InitKlasterEffect(stack, input, output);
  InitHistogram(output);
}

/*----------------------------------------------------------------------*/

function InitEditorFeatures(stack, input, output, inputTexture, outputTexture)
{
  InitSaveFile(output);
  InitCanvasSelection(stack, input, inputTexture, output);
  InitCoordsRendering(input);
  InitCanvasSizeUpdate(inputTexture, outputTexture);
  InitUndoRedoSystem(stack);
}

/*----------------------------------------------------------------------*/

document.addEventListener("DOMContentLoaded", function()
{

   var input = document.getElementById("beforeTexture");
   var output = document.getElementById("afterTexture");

   var inputTexture = new TextureEffect({canvas: input});
   var outputTexture = new TextureEffect({canvas: output});

   var stack = new ActionStack(new Action(inputTexture, outputTexture));

   InitEditorEffectComponents(stack, input, output);
   InitEditorFeatures(stack, input, output, inputTexture, outputTexture);
})
